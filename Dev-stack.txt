1.You can quickly create a separate stack user to run DevStack with

    $ sudo useradd -s /bin/bash -d /opt/stack -m stack

2.Since this user will be making many changes to your system, it should have sudo privileges:

    $ echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack
    $ sudo su - stack

3.Download DevStack

    $ git clone https://git.openstack.org/openstack-dev/devstack
    $ cd devstack

4.The devstack repo contains a script that installs OpenStack and templates for configuration files
  Create a local.conf file with 4 passwords preset at the root of the devstack git repo.

    [[local|localrc]]
    ADMIN_PASSWORD=secret
    DATABASE_PASSWORD=$ADMIN_PASSWORD
    RABBIT_PASSWORD=$ADMIN_PASSWORD
    SERVICE_PASSWORD=$ADMIN_PASSWORD

5.This is the minimum required config to get started with DevStack.
  Start the install

    $./stack.sh

    This will take a 15 - 20 minutes, largely depending on the speed of your internet connection. Many git trees and packages will be installed during this process.

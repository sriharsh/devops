﻿02-August-2016:: finished
--------------------
-deployment of DB scripts to AWS
-Backup and restoration of obe-proddev
-restoration OBE_QA in aws dbserver
-transfered build files to build-server
-faced an error regarding pdf in onboarding, its basically the windows 10 issue.
03-aug:
------
-git doc to HR
-tranfer of nameservers to route53
-working on signature issue for onboarding
04-aug:
------
deployment of ensoulic.com
	
16-aug:
-------
-deployment of digitalization tool to QA server
-transfer of nameservers
-looking for solution for signature issue

17-aug:
-------
-Deployed onboarding application
-Had a call with Godaddy supporting team regarding subdomain issue
-read an article (how to automate port scan using nmap and jenkins)
-Had a call with AWS supporting team
	-configured ELB health check option
	-created three record sets which points to loadbalancer.
-checking of MIME types in QA server regarding signature issue.
-discussion with manish regading signature issue.
18-aug:
------
-had a call with AWS supporting team regarding ssl certificate issue
-build and deploy of timesheet to QA and Dev
-deployment of myboardshare in DEV and QA
-requested for extra domain in the ssl certificate
-Redeployment of boardshare to QA
-taken a call with AWS supporting team regarding multiple domain assigning to ssl where multiple sites hosted in single instance.
-taken boardshare production backup
-Boradshare new changes deployed to DEV
19-aug:
-------
-testing and replacing new ssl certificate with old one for load balancer.
-taken backup and restored production backup in obe-proddev and execution of change scripts.
-deploment of myboardshare in to DEV and QA 
-checking svn-git migration
-session expired error after creating a copy of OBE application.
-creating documentation for AWS-enrollez setup
-boardshare production deployment and execution of SQL queries.
22-aug:
------
-Deployment of boardshare app & DB from prod to Dev(around 40min)
-Build and deployment of timesheet to QA & Dev and execution of db scripts deployment(around 2omin)
-taken timesheet application and database backup from production envoronment.(around 20min)
-timesheet production deployment and db scripts deployment(around 20min)
-preparation of documentation for AWS ensoulic(around 25min)
23-aug:
------
-taken latest copy OBE, Timesheet & OnBoarding applications from QA server to AWS  OBE ,Timesheet & OnBoarding application &
 Execute all updated script(around 30min)
-Active pdf portal application license update(around 15min)
-working on session expired issue(rest)
-checking the error related to svn-git migration(rest)
24-aug:
------
-taken latest backup of production EnrollEZ database And restore Production database backup in “OBE-ProdDev” database on “172.16.1.95\DSRQA” server run all updated & change scripts.(around 35min)
-checking the error related to svn-git migration.(rest)
-checking on session expired issue.(rest)
-given an update to jeffy that session expired issue is not resolved.
25/26-aug:
------
-didnt get any proper solution for svn-git migration error so checking on other ways of svn-git migration and session expiration error too.
29-Aug:
-------
--taken update from SVN and  builded Timesheet applications in QA server and  deployed QA  latest Timesheet application.(15min)
-- deployed timesheet application on PRODUCTION server and deployed that DB scripts.(25min)
--Travel portal deployment.(5min)
--Travel portal deployment again.(5min)
--working on SVN-git migrantion.(rest)

30-Aug:
-------
--Travel portal deployment.(5min)
--taken production DB-backup and restoring in proddev in QA and execution of required scripts(30min).
--We get an branch error, checked on that.(40min)
--SVN-git migration

31-Aug:
------
--Had a call with AWS support team(1hr)
--travel portal deployment(5min)
--Had a discuission regarding visual studio thing with dheeraj and manish.
--CSR-celero deployment
--visual studio installation
--installing debian in virtual box to tried to install github init.
--Deployment of latest build to AWS(ensoul)(15min)
--DB scripts deployment to AWS(ensoul)(5min)

1-Sept:
------
--testing the travel portal,timesheet,obe,onboarding,boardshare build process with VS13.
--SVN-git testing on another repo.
--while syncing newchanges from svn to git repo got an issue, so checked on it.

2-Sept:
------
--Git-lab installation processes.
--travel-portal build and deployment.(15min)
--obe build and deployment to QA & Dev

5-Sept:
------
--Travel-portal build and deployment(20min)
-- to test svn-git migration on build server(installed required tools)
--  build and deployment of obe,timesheet,onboarding to AWS server, deployment db scripts to AWS dbserver.
--Deletion of unwanted data in build server

6-Sept:
------
--travel portal build and deployment.
--deploy this java web application i. e. CSR_Celero on 172 server.
--take latest backup of production EnrollEZ database And restore Production database backup in “OBE-ProdDev” database on “172.16.1.95\DSRQA” server run all updated & change scripts.
7-sept:
------
--installation of jenkins on build server.got some errors so reinstalled again.
--configuration of jenkins.
--installing ant and jdk.
--creating jobs and tested thw build process.
8-sept:
------
--travel portal application build and deployment
--checking of git advanced topics and started to check docker.
9-sept:
------
--taken update from SVN and  build OBE, Timesheet & OnBoarding applications in QA server and  deploy QA  latest OBE ,Timesheet & OnBoarding application 
--build and deployed Travel portal application on dev. Environment
--checking on git topics and reading on other topics related deveops for KT.
12sept:
------
13sept:
------
--taken update from SVN and  build OBE, Timesheet & OnBoarding applications in QA server and  deploy QA  latest OBE ,Timesheet & OnBoarding application 
--got an error after building the code, so dheeraj did some configuration changes and deployed the build from his side(25min).
14-sept:
-------
--update from SVN and  build OBE, Timesheet & OnBoarding applications in QA server and  deploy QA  latest OBE ,Timesheet & OnBoarding application
--deploy the OBE, Timesheet & OnBoarding applications on PRODUCTION server 
--checking on the login issues on hrcmdcentral.com
15-sept:
-------
--holiday
16-sept:
-------
--build and deployment of travel portal application.
--KT about docker.
19-sept:
-------
--taken latest backup of production EnrollEZ database And restored backup in “OBE-ProdDev” database on QA and run all updated & change scripts.(30min)
--created qa environment for travel portal and deployed dev copy in that.(15min)
--checking on docker(fundamentals) self paced training.
20-sept:
-------
--build and deployed Travel Portal Application on dev env(15min)
--created new DB in production server for enrollez-copy application, taken latest production backup and restored to that.
--build and deployment of Travel portal application on dev env again.
--meeting regarding health insurance claims.
--KT regarding docker.
21-sept:
-------
--build and deployment of OBE and onboarding to dev and QA, execution of db scripts.
--build and deployment of travel portal to dev and QA
--KT about docker and reading some online articals regarding devops.
22-sept:
-------
--checking on docker and python basic concepts.
23-sept:
-------
--build and deployment of travel portal to dev and QA
--build and deployment of travel portal to dev and QA again
--anniversary celebrations
--checking on docker and python
--helped sachin in installation of s/w for mechanical team systems.
